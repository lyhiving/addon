# -*- coding: utf-8 -*-
# Part of openerp.hk. See LICENSE file for full copyright and licensing details.##
##################################################################################

{
    'name': '自定义页脚',
    'version': '11.0.0.1',
    'summary': '自定义网站页脚.',
    'author': 'OpenERP.HK',
    'sequence': 1,
    'license': 'AGPL-3',
    'website': 'https://cdn.openerp.hk/',
    'category': 'Website',
       
    'description':"""
    """,
    'depends':[ 'website'],
    'data':[
        'views/assets_view.xml',
        'views/footer_view_spt.xml',
        'views/theme_customize_template_view_spt.xml',
        ],

    'application': True,
    'images': ['static/description/banner.png'],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
