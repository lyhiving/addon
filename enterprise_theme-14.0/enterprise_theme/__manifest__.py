{
    'name': 'Odoo 企业版主题',
    'version': '14.0.1.0.1',
    'summary': 'Odoo 企业版主题样式',
    'author': 'openerp.hk',
    'license': 'AGPL-3',
    'maintainer': 'openerp.hk',
    'company': 'openerp.hk',
    'website': 'https://cdn.openerp.hk',
    'depends': [
        'web'
    ],
    'category':'Branding',
    'description': """
           企业版主题样式
    """,
   'data': [

    'views/webclient_template_extend.xml',

    ],
    'price':0,
    'currency':'USD',
    'installable': True,
    'auto_install': False,
    'application': True,
    'images': ['static/description/icon.png','static/description/main_screenshot.png']
}
