# -*- coding: utf-8 -*-
{
    'name': "button_action_demo",

    'summary': """
        此例子说明如何从Form视图创建action按钮.""",

    'description': """
       此例子说明如何从Form视图创建action按钮.
    """,

    'author': "openerp.hk",
    'website': "http://cdn.openerp.hk",

    # Categories can be used to filter modules in modules listing

    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/button_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo.xml',
    ],
}
