# odoo-wechat by openerp.hk 
# https://cdn.openerp.hk

主要功能：一个实现微信单点登入odoo模块实现

目的：解决odoo当前系统登录，需要使用用户名的问题，直接对接微信API进行单点登录
![odoo微信扫码登录](https://images.gitee.com/uploads/images/2021/0311/113532_21c0de40_4769647.png "wechat_login_scan.png")